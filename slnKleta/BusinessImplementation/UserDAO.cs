﻿using DataImplementation;
using DomainImplementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessImplementation
{
    public class UserDAO
    {
        SqlConnection cn;
        SqlDataAdapter da;
        private string keyEncrypt;

        public UserDAO() {
            cn = new SqlConnection(Connection.getConexion_Kleta);
            keyEncrypt = Connection.getKeyEncrypt;
        }

        public bool CrearUsuario(Usuario u) {
            da = new SqlDataAdapter("sp_UserRegister", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@varNombreUsuario", ((object)u.varNombreUsuario) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varUser", ((object)u.varUser) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varClave", ((object)u.varPassword) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varKeyEncrypt", ((object)keyEncrypt) ?? DBNull.Value);
            try
            {
                cn.Open();
                return da.SelectCommand.ExecuteNonQuery() > 0;
            }
            catch (Exception)
            {
                return false;
                //throw;
            }
            finally {
                cn.Close();
            }
        }

        public bool ValidateUserExists(Usuario u)
        {
            da = new SqlDataAdapter("sp_ValidateUserExists", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@varUser", ((object)u.varUser) ?? DBNull.Value);

            try
            {
                cn.Open();
                DataTable dt = new DataTable();
                da.Fill(dt);
                return Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString()) == 1;
            }
            catch (Exception)
            {
                return false;
                //throw;
            }
            finally {
                cn.Close();
            }
        
        }


        public DataTable ValidateUserLogin(string varLogin, string varPassword)
        {
            da = new SqlDataAdapter("sp_UserLogin", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@varUser", ((object)varLogin) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varPassword", ((object)varPassword) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varKeyEncrypt", ((object)keyEncrypt) ?? DBNull.Value);

            try
            {
                cn.Open();
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
                //throw;
            }
            finally
            {
                cn.Close();
            }

        }





    }
}
