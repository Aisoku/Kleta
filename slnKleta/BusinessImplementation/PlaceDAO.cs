﻿using DataImplementation;
using DomainImplementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;


namespace BusinessImplementation
{
    public class PlaceDAO
    {
        SqlConnection cn;
        SqlDataAdapter da;
        private string keyEncrypt;


        public PlaceDAO(){
             cn = new SqlConnection(Connection.getConexion_Kleta);
            keyEncrypt = Connection.getKeyEncrypt;
        }


        public bool ValidatePlaceRegistered(Place p) {
            da = new SqlDataAdapter("sp_ValidatePlaceRegistered", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@varNamePlace", ((object)p.varNombrePlace) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varNroDocument", ((object)p.varNroDocument) ?? DBNull.Value);
            try
            {
                cn.Open();
                DataTable dt = new DataTable();
                da.Fill(dt);
                return Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString()) == 1;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }

        }
        
        public bool RegisterPlace(Place p)
        {
            da = new SqlDataAdapter("sp_RegisterPoint", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@varNamePlace", ((object)p.varNombrePlace) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varNroDocumento", ((object)p.varNroDocument) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varRazonSocial", ((object)p.varRazonSocial) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varAddress", ((object)p.varAddress) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varCordLong", ((object)p.varCordLogitude) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varCordLat", ((object)p.varCodLatitutde) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varIdtUserRegister", ((object)p.varIdtUserRegister) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varEmail", ((object)p.varEmail) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varPhone", ((object)p.varPhone) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varIdtCategorie", ((object)p.varIdtbCategories) ?? DBNull.Value);
            
            try
            {
                cn.Open();
                return da.SelectCommand.ExecuteNonQuery() > 0;
            }
            catch (Exception)
            {
                return false;
                //throw;
            }
            finally
            {
                cn.Close();
            }

        }
        //sp_RegisterPoint

        public DataTable getCategories() {
            da = new SqlDataAdapter("sp_ListarCategories", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        //
        }

        public DataTable getNearbyPlaces(Place p) {
            da = new SqlDataAdapter("sp_getNearbyPlaces", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@varCordLat", ((object)p.varCodLatitutde) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varCodLong", ((object)p.varCordLogitude) ?? DBNull.Value);
            try
            {
                cn.Open();
                DataTable dt = new DataTable("tbPlaces");
                da.Fill(dt);
                return dt;//DataTableToJSON(dt);
            }
            catch (Exception)
            {
                return null;
                throw;
            }
            finally {
                cn.Close();
            }
        }

        public string DataTableToJSON(DataTable table)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }


    }
}
