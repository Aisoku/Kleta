﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF_Kleta.Errors
{
    [DataContract]
    public class ErroresUsuario
    {
        [DataMember]
        public int intCodError { get; set; }
        [DataMember]
        public string varError { get; set; }
    }
}