﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF_Kleta
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IPlace" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IPlace
    {
        [OperationContract]
        bool ValidatePlace(DomainImplementation.Place p);
        [OperationContract]
        bool RegisterPlace(DomainImplementation.Place p);
        [OperationContract]
        DataTable getNearbyPlaces(DomainImplementation.Place p);
        [OperationContract]
        bool ReservePlace(DomainImplementation.Place p, string msg);
        [OperationContract]
        DataTable getReserves(DomainImplementation.Place p);
    }
}
