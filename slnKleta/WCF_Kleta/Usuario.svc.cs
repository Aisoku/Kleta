﻿using BusinessImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WCF_Kleta.Errors;

namespace WCF_Kleta
{
    
    public class Usuario : IUsuario
    {
        public bool CreateUser(DomainImplementation.Usuario user)
        {
            UserDAO objUser = new UserDAO();
            try
            {
                if (objUser.ValidateUserExists(user))
                {
                    throw new WebFaultException<ErroresUsuario>(new ErroresUsuario()
                    {
                        intCodError=1,
                        varError="El usuario ya existe" 
                    }, HttpStatusCode.Conflict);    
                }
                    return objUser.CrearUsuario(user);
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
    }
}

