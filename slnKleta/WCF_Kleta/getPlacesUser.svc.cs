﻿using DataImplementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;

namespace WCF_Kleta
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "getPlacesUser" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione getPlacesUser.svc o getPlacesUser.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class getPlacesUser : IgetPlacesUser
    {
        SqlConnection cn;
        SqlDataAdapter da;

        public getPlacesUser() {
            cn = new SqlConnection(Connection.getConexion_Kleta);
        }

        public string getPlacesUserPoint(string CordLat, string CordLong)
        {
            da = new SqlDataAdapter("getPlacesPointUser", cn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@varCordLatUser", ((object)CordLat) ?? DBNull.Value);
            da.SelectCommand.Parameters.AddWithValue("@varCodLatitutde", ((object)CordLong) ?? DBNull.Value);
            
            cn.Open();
            try
            {
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return DataTableToJSON(dt);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string DataTableToJSON(DataTable table)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

    }
}
