﻿using BusinessImplementation;
using IronSharp.Core;
using IronSharp.IronMQ;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WCF_Kleta.Errors;

namespace WCF_Kleta
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Place" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Place.svc o Place.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Place : IPlace
    {
        public bool ValidatePlace(DomainImplementation.Place p)
        {
            PlaceDAO objp = new PlaceDAO();
            try
            {
                bool rpt = false;
                if (objp.ValidatePlaceRegistered(p))
                {
                    throw new WebFaultException<ErroresPlace>(new ErroresPlace()
                    {
                        intCodError = 1,
                        varError = "El lugar ya se encuentra registrado"
                    }, HttpStatusCode.Conflict);
                }
                else {
                    rpt = true;
                }
                return rpt;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
            //throw new NotImplementedException();
        }

        public bool RegisterPlace(DomainImplementation.Place p){
            try
            {
                PlaceDAO objp = new PlaceDAO();
                return objp.RegisterPlace(p);
            }
            catch (Exception)
            {
                throw new WebFaultException<ErroresPlace>(new ErroresPlace()
                {
                    intCodError = 409,
                    varError = "Ocurrió un problema con el registo del punto."
                }, HttpStatusCode.Conflict);
                //return false;
                throw;
            }
        }


        public DataTable getNearbyPlaces(DomainImplementation.Place p)
        {
            try
            {
                PlaceDAO objp = new PlaceDAO();
                DataTable tbPlaces = new DataTable("tbPlaces");
                tbPlaces = objp.getNearbyPlaces(p);
                return tbPlaces;
            }
            catch (Exception)
            {
                
                throw;
            }
        }


        public bool ReservePlace(DomainImplementation.Place p, string msg)
        {
            var ironMq = IronSharp.IronMQ.Client.New(new IronClientConfig
            {
                ProjectId = "5babd432c72ec60009bde7ef",
                Token = "cQESyoHddg4xSgMpTB7b",
                Host = "mq-aws-eu-west-1-1.iron.io",
                Scheme = "http",
                Port = 80
            });

            QueueClient queue = ironMq.Queue(p.varIdPLace);
            try
            {
                string messageId = queue.Post(msg);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            } 
        }





        public DataTable getReserves(DomainImplementation.Place p)
        {
            DataTable dt = new DataTable("dtReserve");
            dt.Columns.Add("message", typeof(int));
            
            var ironMq = IronSharp.IronMQ.Client.New(new IronClientConfig
            {
                ProjectId = "5babd432c72ec60009bde7ef",
                Token = "cQESyoHddg4xSgMpTB7b",
                Host = "mq-aws-eu-west-1-1.iron.io",
                Scheme = "http",
                Port = 80
            });

            QueueClient queue = ironMq.Queue(p.varIdPLace);
            QueueInfo info = queue.Info();
            var a = info.Size;
            
            IronSharp.IronMQ.MessageCollection messages = queue.Peek(a);
            for (int i = 0; i < a; i++)
            {
                dt.Rows.Add(new Object[]{
                messages.Messages[i].Body.ToString()
                });
                
            }
            return dt;
        }
    }
}
