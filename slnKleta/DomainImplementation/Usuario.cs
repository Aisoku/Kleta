﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DomainImplementation
{
    [DataContract]
    public class Usuario
    {
        [DataMember]
        public string varIdtUsuario { get; set; }
        [DataMember]
        public string varNombreUsuario { get; set; }
        [DataMember]
        public string varUser { get; set; }
        [DataMember]
        public string varPassword { get; set; }
    }
}