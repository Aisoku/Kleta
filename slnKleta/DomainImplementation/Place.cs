﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DomainImplementation
{
    public class Place
    {
        [DataMember]
        public string varNombrePlace {get; set;}
        [DataMember]
        public string varNroDocument {get; set;}
        [DataMember]
        public string varRazonSocial {get; set;}
        [DataMember]
        public string varIdtUserRegister { get; set; }
        [DataMember]
        public string varAddress { get; set;}
        [DataMember]
        public string varEmail {get;set;}
        [DataMember]
        public string varPhone {get; set;}
        [DataMember]
        public string varCordLogitude{get; set;}
        [DataMember]
        public string varCodLatitutde{get; set;}
        [DataMember]
        public string varIdtbCategories { get; set; }
        [DataMember]
        public string varDescriptionCat	{get; set;}
        [DataMember]
        public string varColor { get; set; }
        [DataMember]
        public string varFechaRegistro { get; set; }
        [DataMember]
        public string varUrlFoto { get; set; }
        [DataMember]
        public string varIdPLace { get; set; }

    }
}
