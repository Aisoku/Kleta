﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataImplementation
{
    public class Connection
    {
        public static string getConexion_Kleta
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["cnKleta"].ConnectionString;
            }
        }

        public static string getKeyEncrypt
        {
            get
            {
                return ConfigurationManager.AppSettings["keyEncrypt"];
            }
        }
    }
}
